from typing import Optional
import bpy
import math
# Thanks CGArtPython person : https://github.com/CGArtPython/bpy_building_blocks_examples/blob/main/clean_scene/clean_scene_example_1.py
def clean_scene():
    """
    TODO : makes this not use ops
    Removing all of the objects, collection, materials, particles,
    textures, images, curves, meshes, actions, nodes, and worlds from the scene
    """
    if bpy.context.active_object and bpy.context.active_object.mode == "EDIT":
        bpy.ops.object.editmode_toggle()

    for obj in bpy.data.objects:
        obj.hide_set(False)
        obj.hide_select = False
        obj.hide_viewport = False

    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete()

    collection_names = [col.name for col in bpy.data.collections]
    for name in collection_names:
        bpy.data.collections.remove(bpy.data.collections[name])

    # in the case when you modify the world shader
    world_names = [world.name for world in bpy.data.worlds]
    for name in world_names:
        bpy.data.worlds.remove(bpy.data.worlds[name])
    # create a new world data block
    bpy.ops.world.new()
    bpy.context.scene.world = bpy.data.worlds["World"]

    if bpy.app.version >= (3, 0, 0):
        bpy.ops.outliner.orphans_purge(
            do_local_ids=True, do_linked_ids=True, do_recursive=True
        )
    else:
        # call purge_orphans() recursively until there are no more orphan data blocks to purge
        result = bpy.ops.outliner.orphans_purge()
        if result.pop() != "CANCELLED":
            purge_orphans()

class GreasePencil:
    """makes a grease pencil object and layer, encapsulate them
    in a data structure and uses them to draw"""

    # this remembers the number of grease pencil created to
    # get incrementing names
    _grease_pencil_num = 0
    # list of (x, y, z)
    stroke_coordinate = list[tuple[float, float, float]]
    # list of (pressure, strenght)
    pressure_strength = list[tuple[float, float]]
    def __init__(self, name="grease_pencil_{}"):
        """name: pencil name,"""
        self.pencil_name = name.format(GreasePencil._grease_pencil_num) if name == "grease_pencil_{}" else name
        self.layer_name = "grease_pencil_layer_{}".format(GreasePencil._grease_pencil_num) if name == "grease_pencil_{}" else name + "_layer"
        self.grease_pencil = bpy.data.grease_pencils.new(self.pencil_name)
        # dont know if set_active=True does anything
        self.grease_layer = self.grease_pencil.layers.new(self.layer_name, set_active=True)
        self.current_frame_index = -1
        self.next_frame()
        # increment GreasePencil._grease_pencil_num if we used it to name a pencil
        if name == "grease_pencil_{}":
            GreasePencil._grease_pencil_num+=1
        self.is_commited = False

    def next_frame(self, frame_advance=1):
        self.current_frame_index += frame_advance
        self.current_frame = self.grease_layer.frames.new(self.current_frame_index)

    def commit(self):
        if not self.is_commited:
            self.grease_obj = bpy.data.objects.new(self.grease_pencil.name, self.grease_pencil)
            bpy.context.collection.objects.link(self.grease_obj)
        else:
            return


    def draw_curve(
            self,
            point_coords: list[stroke_coordinate],
            points_pressure_strength: Optional[list[pressure_strength]] = None) :
        points_pressure_strength = [(1.0, 1.0)] * len(point_coords) if points_pressure_strength is None else points_pressure_strength
        pps_len =len(points_pressure_strength)
        # cyclically repeat points_pressure_strength until its longer or equal to point_coords and then trim
        # to the right len
        points_pressure_strength = points_pressure_strength * math.ceil(len(point_coords)/len(points_pressure_strength))
        # trim to the length of
        points_pressure_strength = points_pressure_strength[0:len(point_coords)]
        # makes the stroke
        stroke = self.current_frame.strokes.new()
        stroke.display_mode = '3DSPACE'
        for i, point_coord in enumerate(point_coords):
            stroke.points.add(len(point_coords), pressure=points_pressure_strength[i][0], strength=points_pressure_strength[i][1])
            stroke.points[i].co = point_coord
        return stroke
